

Heavily based on the Vertical Fixed Navigation library at http://codyhouse.co/gem/vertical-fixed-navigation/

Article on CodyHouse (http://codyhouse.co/?p=226)!
Terms (http://codyhouse.co/terms/)! !

Vertical Fixed Navigation integrates Eric Meyer’s CSS reset rules (http://meyerweb.com/eric/tools/css/reset/) and
Modernizr (http://modernizr.com/).

Installation Instructions

1) Move vertical-fixed-navigation folder to sites/all/libraries/
2) Add file from example_modernizr_file to sites/all/libraries/modernizr
3) Navigate to /admin/structure/block and configure the Vertical Fixed Navigation example block to appear on whichever pages you'd like.
4) You can create as many blocks as you like, so long as you include the same markup.
5) To override the default CSS, add to the css/vertical-fixed-navigation-overrides.css file.
